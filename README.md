Digitale Lebensverwaltung ist ein Softwareprojekt mit dem Ziel, eine Software zu entwickeln, die alltägliche Verwaltungsaufgaben erleichtert. Dazu gehören beispielsweise folgende Fähigkeiten: (in Planung)


- Übersicht über abgeschlossene Verträge und deren Kündigungsfristen
- Übersicht über erteilte SEPA-Lastschriftmandate
- Vereinfachung des Umzugs: Erstellen von Umzugsmeldungen an Institutionen, welchen der Umzug mitgeteilt werden muss.

Viele weitere Fähigkeiten sind denkbar...

